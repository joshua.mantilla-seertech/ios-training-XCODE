//
//  AssetRequestTableViewCell.swift
//  AssetManagement
//
//  Created by Seer on 26/07/2018.
//  Copyright © 2018 Seer. All rights reserved.
//

import UIKit

class AssetRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var requesterLabel: UILabel!
    @IBOutlet weak var assetNameLabel: UILabel!
    @IBOutlet weak var assetDetailLabel: UILabel!
    @IBOutlet weak var assetIsApproved: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
