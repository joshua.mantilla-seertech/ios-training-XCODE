//
//  ViewController.swift
//  AssetManagement
//
//  Created by Seer on 17/07/2018.
//  Copyright © 2018 Seer. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    let defaults = UserDefaults.standard
    var urlString = "http://localhost:8000/users"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let userLogged = defaults.string(forKey: "userLoggedIn")
        
        if userLogged != "" || userLogged != nil{
            
            performSegue(withIdentifier: "goToAssetPage", sender: self)
            
        }
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    @IBAction func loginClicked(_ sender: Any) {
        print("login clicked")
        
        self.login(username: userNameField.text!, password: passwordField.text!)
    }
    
    
    func login(username: String ,password: String){
        if let url = URL(string: urlString + "?username=" + username){
            print("WILL NOW API CALL ---- ")
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                print("responsE! \(response)")
                print("DATA \(data)")
                if error != nil {
                    print(error ?? "")
                } else {
                    if let usableData = data {
                        
                        if let userList = try? JSONSerialization.jsonObject(with: usableData, options:[]) as? NSArray ?? [] {
                            if userList.count == 0{
                                OperationQueue.main.addOperation {
                                    let alert = UIAlertController(title: "Login Failed", message: "Username or Password is invalid", preferredStyle: UIAlertControllerStyle.alert)
                                    
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                    
                                    self.present(alert, animated: true, completion: nil)
                                }

                            }else{
                                
                                for userItem in userList{
                                    if let userDict = userItem as? NSDictionary{
                                        print("userDict")
                                        print(userDict)
                                        let username:String = userDict.value(forKey: "username") as! String
                                        let password:String = userDict.value(forKey: "password") as! String
                                        let userId = userDict.value(forKey: "id")
                                        
                                        if username == password{
                                            
                                            
                                            OperationQueue.main.addOperation {
                                                self.defaults.set(username, forKey: "userLoggedIn")
                                                self.defaults.set(userId, forKey: "userLoggedInId")
                                                print("go to next page")
                                                self.performSegue(withIdentifier: "goToAssetPage", sender: self)
                                            }
                                            
                                        }
                                        else{
                                            
                                            OperationQueue.main.addOperation {
                                                let alert = UIAlertController(title: "Login Failed", message: "Username or Password is invalid", preferredStyle: UIAlertControllerStyle.alert)
                                                
                                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                                
                                                self.present(alert, animated: true, completion: nil)
                                            }
                                            
                                            
                                        }
                                        
                                        
                                    }
                                    
                                }
                                
                            }
                        }
                        
                        
                        
                    }
                }
            }
            task.resume()
            
            
        }else{
            
        }
    }

}

