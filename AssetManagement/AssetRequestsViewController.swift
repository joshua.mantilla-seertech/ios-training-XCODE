//
//  AssetRequestsViewController.swift
//  AssetManagement
//
//  Created by Seer on 26/07/2018.
//  Copyright © 2018 Seer. All rights reserved.
//

import UIKit

class AssetRequestsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    let defaults = UserDefaults.standard
    
    @IBOutlet weak var assetRequestTableView: UITableView!
    
    @IBOutlet weak var loggedInUserLabel: UILabel!
    let assetRequestURL = URL(string:"http://localhost:8000/asset-requests/")
    
    
    var assetRequestListData = [AssetRequestList]()
    struct AssetRequestList {
        let requester : String
        let requesterId : NSNumber
        let assetId : NSNumber
        let name : String
        let details : String
        let requestId : NSNumber
        let assetIsApproved : Bool
        
        
        init(dictionary: NSDictionary) {
            if let userData = dictionary.value(forKey: "user") {
                
                if let requester = (userData as AnyObject).value(forKey: "username") {
                
                    self.requester = requester as! String
                    
                }
                else{
                    self.requester = ""
                }
                
                if let requesterId = (userData as AnyObject).value(forKey: "id") {
                    
                    self.requesterId = requesterId as! NSNumber
                    
                }
                else{
                    self.requesterId = -1
                }

                
            }else{
                self.requester = ""
                self.requesterId = -1
            }
            
            if let assetData = dictionary.value(forKey: "asset") {
                
                if let assetName = (assetData as AnyObject).value(forKey: "name"){
                    
                    self.name = assetName as! String
                    
                }else{
                    self.name = ""
                }
                
                if let assetDetails = (assetData as AnyObject).value(forKey: "details"){
                    
                    self.details = assetDetails as! String
                    
                }else{
                    self.details = ""
                }
                
                if let assetId = (assetData as AnyObject).value(forKey: "id"){
                    
                    self.assetId = assetId as! NSNumber
                    
                }else{
                    self.assetId = -1
                }
                
            }else{
                self.name = ""
                self.details = ""
                self.assetId = -1
            }
            
            if let requestId = dictionary.value(forKey: "id") {
                self.requestId = requestId as! NSNumber
            }else{
                self.requestId = -1
            }
            
            
            if let assetIsApproved = dictionary.value(forKey: "is_approved") {
                self.assetIsApproved = assetIsApproved as! Bool
            }else{
                self.assetIsApproved = false
            }
        }
    }
    
    
    @IBOutlet weak var assetRequestsSearchBar: UISearchBar!
    var filteredData = [AssetRequestList]()
    var isSearching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assetRequestsSearchBar.delegate = self
        assetRequestsSearchBar.returnKeyType = UIReturnKeyType.done
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let userLogged = defaults.string(forKey: "userLoggedIn")
        
        if userLogged != "" || userLogged != nil{
            
            loggedInUserLabel.text = userLogged
            
        }
        
        self.loadRequestAssetTable()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearching{
            return filteredData.count
        }
        
        return assetRequestListData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = assetRequestTableView.dequeueReusableCell(withIdentifier: "assetRequestCell") as! AssetRequestTableViewCell
        
        
        if isSearching {
            
            cell.requesterLabel.text = filteredData[indexPath.row].requester
            cell.assetNameLabel.text = filteredData[indexPath.row].name
            cell.assetDetailLabel.text = filteredData[indexPath.row].details
            cell.assetIsApproved.text = filteredData[indexPath.row].assetIsApproved ? "True" : "False"
            
        }
        else{
            
            cell.requesterLabel.text = assetRequestListData[indexPath.row].requester
            cell.assetNameLabel.text = assetRequestListData[indexPath.row].name
            cell.assetDetailLabel.text = assetRequestListData[indexPath.row].details
            cell.assetIsApproved.text = assetRequestListData[indexPath.row].assetIsApproved ? "True" : "False"
            
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        print("FOR EDITING TABLE VIEW FOUND!")
        print(indexPath[1])
        print(assetRequestListData[indexPath[1]])
        print(tableView)
        
        let assetData = assetRequestListData[indexPath[1]]
        
        let approveOption = UITableViewRowAction(style: .normal, title: "Approve") { action, index in
            print("approve button tapped for \(assetData)")
            self.approveRejectRequest(isApprove: true, assetId: assetData.assetId, requestId: assetData.requestId, requesterId: assetData.requesterId, requester: assetData.requester, details: assetData.details)
        }
        approveOption.backgroundColor = .green
        
        let rejectOption = UITableViewRowAction(style: .normal, title: "Reject") { action, index in
            print("reject button tapped \(assetData)")
            self.showNotifs(title: "Reject request assets success!", message: "Asset request rejected", goToAssetPage: false)
        }
        rejectOption.backgroundColor = .red
        
        
        return [approveOption, rejectOption]
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == ""{
            
            isSearching = false
            view.endEditing(true)
            assetRequestTableView.reloadData()
        }else{
            isSearching = true
            filteredData = assetRequestListData.filter({
                $0.name.localizedCaseInsensitiveContains(searchBar.text!) ||
                $0.details.localizedCaseInsensitiveContains(searchBar.text!) ||
                $0.requester.localizedCaseInsensitiveContains(searchBar.text!)
            })
            
            assetRequestTableView.reloadData()
        }
    }
    
    func loadRequestAssetTable(){
        if let url = assetRequestURL {
            print("WILL NOW API CALL ---- ")
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                
                if error != nil {
                    print(error ?? "")
                } else {
                    if let usableData = data {
                        
                        if let assetRequestList = try? JSONSerialization.jsonObject(with: usableData, options:[]) as? NSArray ?? [] {
                            for assetRequest in assetRequestList{
                                if let assetRequestDict = assetRequest as? NSDictionary{
                                    self.assetRequestListData.append(AssetRequestList(dictionary: assetRequestDict))
                                    OperationQueue.main.addOperation {
                                        self.assetRequestTableView.reloadData()
                                        //   self.UITableView_Auto_Height();
                                    }
                                    
                                }
                                
                            }
                        }
                        
                        
                        
                        
                    }
                }
            }
            task.resume()
            
            
        }
    }
    
    func approveRejectRequest(isApprove: Bool, assetId: NSNumber, requestId: NSNumber, requesterId: NSNumber, requester: String, details: String){
        _ = defaults.integer(forKey: "userLoggedInId")
        
        let assetData:Any = [
            "is_approved": isApprove,
            "user": requesterId as NSNumber,
            "asset": assetId,
        ]
        let approveUrl = URL(string:"http://localhost:8000/asset-requests/\(requestId)/")
        do {
            
            let jsonData = try JSONSerialization.data(withJSONObject: assetData, options: [])
            let session = URLSession.shared
            let request = NSMutableURLRequest(url: approveUrl!)
            request.httpMethod = "PUT"
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonData
            
            
            let task = session.dataTask(with: request as URLRequest){ data,response,error in
                if error != nil{
                    self.showNotifs(title: "Approve request assets failed!", message: "Something went wrong", goToAssetPage: false)
                    return
                }else{
                    print("PRINTING RESPONSE IN SUBMIT ------")
                    print(data ?? "")
                    print(response ?? "")
                    print(error ?? "")
                    OperationQueue.main.addOperation {
                        
                        self.changeOwner(requester: requester, details: details, assetId: assetId, requesterId: requesterId)
                    }
                }
            }
            task.resume()
        } catch {
            self.showNotifs(title: "Approve request failed!", message: "Unknown error occured", goToAssetPage: false)
        }

        
    }
    
    func changeOwner(requester: String, details: String, assetId: NSNumber, requesterId: NSNumber){
        
        let assetData:Any = [
            "owner": requesterId,
            "username": requester,
            "details": details
            ]
        let approveUrl = URL(string:"http://localhost:8000/assets/\(assetId)/")
        do {
            
            let jsonData = try JSONSerialization.data(withJSONObject: assetData, options: [])
            let session = URLSession.shared
            let request = NSMutableURLRequest(url: approveUrl!)
            request.httpMethod = "PUT"
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonData
            
            
            let task = session.dataTask(with: request as URLRequest){ data,response,error in
                if error != nil{
                    self.showNotifs(title: "Approve request assets failed!", message: "Something went wrong", goToAssetPage: false)
                    return
                }else{
                    print("PRINTING RESPONSE IN Change Owner ------")
                    if let assetList = try? JSONSerialization.jsonObject(with: data!, options:[]) as? NSDictionary {
                        print("PARSED DATA")
                        print(assetList)
                    }
                    print(data ?? "")
                    print(response ?? "")
                    print(error ?? "")
                    OperationQueue.main.addOperation {
                        self.showNotifs(title: "Approve request success!", message: "Approved", goToAssetPage: true)
                    }
                }
            }
            task.resume()
        } catch {
            self.showNotifs(title: "Approve request failed!", message: "Unknown error occured", goToAssetPage: false)
        }
        
        
    }
    
    func showNotifs(title: String, message: String, goToAssetPage: Bool){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction!) in
            
            if goToAssetPage{
                self.performSegue(withIdentifier: "goToAssetPageBack", sender: self)
            }
            
            
        }))
        
        
        self.present(alert, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
