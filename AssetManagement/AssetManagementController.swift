//
//  AssetManagementController.swift
//  AssetManagement
//
//  Created by Seer on 18/07/2018.
//  Copyright © 2018 Seer. All rights reserved.
//

import UIKit


class AssetManagementController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    @IBOutlet weak var lblUserLoggedIn: UILabel!
    let defaults = UserDefaults.standard
    let urlString = URL(string:"http://localhost:8000/assets/")
    let assetRequestURL = URL(string:"http://localhost:8000/asset-requests/")
    @IBOutlet weak var assetsTableView: UITableView!
    @IBOutlet weak var assetsStackView: UIStackView!
    
    @IBOutlet weak var addAssetButton: UIButton!
    @IBOutlet weak var viewAssetRequests: UIButton!
    var isAdmin = false
    
    
    struct AssetList {
        let owner : String
        let name : String
        let details : String
        let image_url : String
        let assetId : NSNumber
        
        init(dictionary: NSDictionary) {
            if let owner = dictionary.value(forKey: "username") {
                self.owner = owner as! String
            }else{
                self.owner = ""
            }
            
            if let assetItem = dictionary.value(forKey: "name") {
                self.name = assetItem as! String
            }else{
                self.name = ""
            }
            
            if let assetItemDetails = dictionary.value(forKey: "details") {
                self.details = assetItemDetails as! String
            }else{
                self.details = ""
            }
            
            if let assetItemImageURL = dictionary.value(forKey: "image_url") {
                self.image_url = assetItemImageURL as! String
            }else{
                self.image_url = ""
            }
            
            if let assetId = dictionary.value(forKey: "id") {
                self.assetId = assetId as! NSNumber
            }else{
                self.assetId = -1
            }
        }
    }
    
    var assetListData = [AssetList]()
    var filteredData = [AssetList]()
    var isSearching = false
    
    @IBOutlet weak var assetsSearchBar: UISearchBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        assetsSearchBar.delegate = self
        assetsSearchBar.returnKeyType = UIReturnKeyType.done
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func UITableView_Auto_Height()
    {
        if(self.assetsTableView.contentSize.height < self.assetsTableView.frame.height){
            var frame: CGRect = self.assetsTableView.frame;
            frame.size.height = self.assetsTableView.contentSize.height;
            self.assetsTableView.frame = frame;
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let userLogged = defaults.string(forKey: "userLoggedIn")
        
        if userLogged != "" || userLogged != nil{
            
            lblUserLoggedIn.text = userLogged
            isAdmin = userLogged == "admin"
            
            if !isAdmin{
                
                addAssetButton.isHidden = true
                viewAssetRequests.isHidden = true
                
            }
        }
        
        self.loadAssetTable()
    }
    
    
    @IBAction func addAssetAction(_ sender: Any) {
        
        //performSegue(withIdentifier: "goToViewAsset", sender: self)
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearching{
            return filteredData.count
        }
        return assetListData.count
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = assetsTableView.dequeueReusableCell(withIdentifier: "assetCell") as! AssetTableViewCell
      
        
        if isSearching{
            
            var imageURLString = filteredData[indexPath.row].image_url
            
            if imageURLString == ""{
            
                imageURLString = "https://images-na.ssl-images-amazon.com/images/I/71HG3D8-OQL._SL1350_.jpg"
                
            }
            
            let imageUrl:URL = URL(string: imageURLString)!
            
            DispatchQueue.global(qos: .userInitiated).async {
                
                let imageData:NSData = NSData(contentsOf: imageUrl)!
                
                // When from background thread, UI needs to be updated on main_queue
                DispatchQueue.main.async {
                    let image = UIImage(data: imageData as Data)
                    cell.assetImageView.image = image
                }
            }

            cell.ownerLabel.text = filteredData[indexPath.row].owner == lblUserLoggedIn.text ? assetListData[indexPath.row].owner + " - YOU" : assetListData[indexPath.row].owner
            cell.nameLabel.text = filteredData[indexPath.row].name
            cell.detailsLabel.text = filteredData[indexPath.row].details
            
        }
        else{
            
            var imageURLString = String(assetListData[indexPath.row].image_url)
            
            if imageURLString == ""{
                
                imageURLString = "https://images-na.ssl-images-amazon.com/images/I/71HG3D8-OQL._SL1350_.jpg"
                
            }
            
            let imageUrl:URL = URL(string: imageURLString!)!
            
            DispatchQueue.global(qos: .userInitiated).async {
                
                let imageData:NSData = NSData(contentsOf: imageUrl)!
                
                // When from background thread, UI needs to be updated on main_queue
                DispatchQueue.main.async {
                    let image = UIImage(data: imageData as Data)
                    cell.assetImageView.image = image
                }
            }
            
            cell.ownerLabel.text = assetListData[indexPath.row].owner == lblUserLoggedIn.text ? assetListData[indexPath.row].owner + " - YOU" : assetListData[indexPath.row].owner
            cell.nameLabel.text = assetListData[indexPath.row].name
            cell.detailsLabel.text = assetListData[indexPath.row].details
            
        }
        
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        var actionOptions = [UITableViewRowAction(style: .normal, title: "No Action"){ action, index in
            }]
        if(assetListData[indexPath[1]].owner == defaults.string(forKey: "userLoggedIn") && !isAdmin){
            
            // do nothing
            
        }else{
            if(isAdmin){
                
                let assetData = assetListData[indexPath[1]]
                
                let deleteOption = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
                    self.deleteAsset(assetId: assetData.assetId)
                }
                deleteOption.backgroundColor = .red
                
                actionOptions = [deleteOption]
            }else{
                
                let assetData = assetListData[indexPath[1]]
                
                let requestOption = UITableViewRowAction(style: .normal, title: "Request Asset") { action, index in
                    
                    self.requestAsset(assetId: assetData.assetId)
                    
                    print("edit button tapped for \(assetData)")
                }
                requestOption.backgroundColor = .green
                
                actionOptions = [requestOption]
            }
        }
        
        
        return actionOptions
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == ""{
            
            isSearching = false
            view.endEditing(true)
            assetsTableView.reloadData()
        }else{
            isSearching = true
            filteredData = assetListData.filter({
                $0.name.localizedCaseInsensitiveContains(searchBar.text!) ||
                $0.owner.localizedCaseInsensitiveContains(searchBar.text!) ||
                $0.details.localizedCaseInsensitiveContains(searchBar.text!)
            })
            
            assetsTableView.reloadData()
        }
    }
    

    func requestAsset(assetId: NSNumber){
        let userId = defaults.integer(forKey: "userLoggedInId")
        let assetRequestData:[String:NSNumber] = [
            "user": userId as NSNumber? ?? 1,
            "asset": assetId
        ]
        
        do {
            
            let jsonData = try JSONSerialization.data(withJSONObject: assetRequestData, options: [])
            let session = URLSession.shared
            let request = NSMutableURLRequest(url: assetRequestURL!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonData
            
            
            let task = session.dataTask(with: request as URLRequest){ data,response,error in
                if error != nil{
                    self.showNotifs(title: "Request asset request failed!", message: "Something went wrong")
                    return
                }else{
                    print("PRINTING RESPONSE IN SUBMIT ------")
                    print(data ?? "")
                    print(response ?? "")
                    print(error ?? "")
                    OperationQueue.main.addOperation {
                        self.showNotifs(title: "Asset requested!", message: "Please wait for admin approval, thank you!")
                    }
                }
            }
            task.resume()
        } catch {
            self.showNotifs(title: "Request asset failed!", message: "Unknown error occured")
        }
        
    }
    
    
    func loadAssetTable(){
        if let url = urlString {
            print("WILL NOW API CALL ---- ")
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                
                if error != nil {
                    print(error ?? "")
                } else {
                    if let usableData = data {
                        
                        if let assetList = try? JSONSerialization.jsonObject(with: usableData, options:[]) as? NSArray ?? [] {
                            for asset in assetList{
                                if let assetDict = asset as? NSDictionary{
                                    self.assetListData.append(AssetList(dictionary: assetDict))
                                    OperationQueue.main.addOperation {
                                        self.assetsTableView.reloadData()
                                     //   self.UITableView_Auto_Height();
                                    }
                                    
                                }
                                
                            }
                        }
                        
                        
                        
                        
                    }
                }
            }
            task.resume()
            
            
        }
    }
    
    func deleteAsset(assetId: NSNumber){
        let assetRequestDeleteURL = URL(string:"http://localhost:8000/assets/\(assetId)/")
        do {
            let session = URLSession.shared
            let request = NSMutableURLRequest(url: assetRequestDeleteURL!)
            request.httpMethod = "DELETE"
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            
            let task = session.dataTask(with: request as URLRequest){ data,response,error in
                if error != nil{
                    self.showNotifs(title: "Delete asset request failed!", message: "Something went wrong")
                    return
                }else{
                    print("PRINTING RESPONSE IN SUBMIT ------")
                    print(data ?? "")
                    print(response ?? "")
                    print(error ?? "")
                    OperationQueue.main.addOperation {
                        self.showDeleteNotifs(title: "Asset deleted!", message: "Delete asset success!")
                    }
                }
            }
            task.resume()
        } catch {
            self.showNotifs(title: "Delete asset failed!", message: "Unknown error occured")
        }
        
    }

    
    @IBAction func logoutAction(_ sender: Any) {
        defaults.removeObject(forKey: "userLoggedIn")
        performSegue(withIdentifier: "goToLogout", sender: self)
        
    }
    
    
    func showNotifs(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func showDeleteNotifs(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction!) in
            
            self.assetListData.removeAll()
            self.assetsTableView.reloadData()
            self.loadAssetTable()
            
            
            }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
}
