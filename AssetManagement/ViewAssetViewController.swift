//
//  ViewAssetViewController.swift
//  AssetManagement
//
//  Created by Seer on 20/07/2018.
//  Copyright © 2018 Seer. All rights reserved.
//

import UIKit

class ViewAssetViewController: UIViewController {
    
    @IBOutlet weak var assetNameField: UITextField!
    @IBOutlet weak var assetDetailsField: UITextField!
    let urlString = URL(string:"http://localhost:8000/assets/")
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var assetURL: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelAddAssetAction(_ sender: Any) {
        
        performSegue(withIdentifier: "goToAssetPageFromView", sender: self)
        
    }
    
    @IBAction func submitAssetAction(_ sender: Any) {
        
        let assetName = assetNameField.text ?? ""
        let assetDetails = assetDetailsField.text ?? ""
        let assetImageURL = assetURL.text ?? ""
        let userLogged = defaults.string(forKey: "userLoggedInId")
        if assetName != "" && assetDetails != ""{
            
            let assetData:Any = [
                "name": assetName,
                "details": assetDetails,
                "image_url": assetImageURL,
                "owner": userLogged
            ]
            
            do {
                
                let jsonData = try JSONSerialization.data(withJSONObject: assetData, options: [])
                let session = URLSession.shared
                let request = NSMutableURLRequest(url: urlString!)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Accept")
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.httpBody = jsonData
                
                
                let task = session.dataTask(with: request as URLRequest){ data,response,error in
                    if error != nil{
                        self.showNotifs(title: "Add assets failed!", message: "Please input asset name and details")
                        return
                    }else{
                        print("PRINTING RESPONSE IN SUBMIT ------")
                        print(data ?? "")
                        print(response ?? "")
                        print(error ?? "")
                        OperationQueue.main.addOperation {
                            self.assetNameField.text = ""
                            self.assetDetailsField.text = ""
                            self.showNotifs(title: "Add assets success!", message: "Successfully added")
                        }
                    }
                }
                task.resume()
            } catch {
                self.showNotifs(title: "Add assets failed!", message: "Unknown error occured")
            }
            
            
        }else{
            self.showNotifs(title: "Add assets failed!", message: "Please input asset name and details")
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func showNotifs(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction!) in
            
            //self.performSegue(withIdentifier: "goToAssetPageFromView", sender: self)
            
        
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

}
