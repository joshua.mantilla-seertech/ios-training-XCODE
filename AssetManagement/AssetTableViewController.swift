//
//  AssetTableViewController.swift
//  AssetManagement
//
//  Created by Seer on 22/07/2018.
//  Copyright © 2018 Seer. All rights reserved.
//

import UIKit

class AssetTableViewController: UITableViewController {
    
    @IBOutlet var assetTableView2: UITableView!
    
    let urlString = URL(string:"http://localhost:8000/assets/")
    struct AssetList {
        let owner : String
        let name : String
        let details : String
        
        init(dictionary: NSDictionary) {
            if let owner = dictionary.value(forKey: "username") {
                self.owner = owner as! String
            }else{
                self.owner = ""
            }
            
            if let assetItem = dictionary.value(forKey: "name") {
                self.name = assetItem as! String
            }else{
                self.name = ""
            }
            
            if let assetItemDetails = dictionary.value(forKey: "details") {
                self.details = assetItemDetails as! String
            }else{
                self.details = ""
            }
            
        }
    }
    var assetListData = [AssetList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadAssetTable()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return assetListData.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return assetListData.count
    }
    
    func loadAssetTable(){
        if let url = urlString {
            print("WILL NOW API CALL ---- ")
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                
                if error != nil {
                    print(error ?? "")
                } else {
                    if let usableData = data {
                        
                        if let assetList = try? JSONSerialization.jsonObject(with: usableData, options:[]) as? NSArray ?? [] {
                            for asset in assetList{
                                if let assetDict = asset as? NSDictionary{
                                    print("assetDict")
                                    print(assetDict)
                                    self.assetListData.append(AssetList(dictionary: assetDict))
                                    print(self.assetListData)
                                    OperationQueue.main.addOperation {
                                        self.assetTableView2.reloadData()
                                        //   self.UITableView_Auto_Height();
                                    }
                                    
                                }
                                
                            }
                        }
                        
                        
                        
                        
                    }
                }
            }
            task.resume()
            
            
        }
    }


    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = assetTableView2.dequeueReusableCell(withIdentifier: "assetCellIdentifier") as! AssetTableViewCell
        print("CALLING TABLE VIEW")
        print(assetListData[indexPath.row])
        cell.assetOwnerLabel
            .text = assetListData[indexPath.row].owner
        cell.assetNameLabel.text = assetListData[indexPath.row].name
        cell.assetDetailsLabel.text = assetListData[indexPath.row].details
        
        
        
        return cell
    
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        // 1
        let shareAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Share" , handler: { (action:UITableViewRowAction, indexPath: IndexPath) -> Void in
            // 2
            let shareMenu = UIAlertController(title: nil, message: "Share using", preferredStyle: .actionSheet)
            
            let twitterAction = UIAlertAction(title: "Twitter", style: UIAlertActionStyle.default, handler: nil)
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
            
            shareMenu.addAction(twitterAction)
            shareMenu.addAction(cancelAction)
            
            self.present(shareMenu, animated: true, completion: nil)
        })
        // 3
        let rateAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Rate" , handler: { (action:UITableViewRowAction, indexPath:IndexPath) -> Void in
            // 4
            let rateMenu = UIAlertController(title: nil, message: "Rate this App", preferredStyle: .actionSheet)
            
            let appRateAction = UIAlertAction(title: "Rate", style: UIAlertActionStyle.default, handler: nil)
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
            
            rateMenu.addAction(appRateAction)
            rateMenu.addAction(cancelAction)
            
            self.present(rateMenu, animated: true, completion: nil)
        })
        // 5
        return [shareAction,rateAction]
    }
}
