//
//  Asset2TableViewCell.swift
//  AssetManagement
//
//  Created by Seer on 22/07/2018.
//  Copyright © 2018 Seer. All rights reserved.
//

import UIKit

class Asset2TableViewCell: UITableViewCell {

    @IBOutlet weak var assetDetailsLabel: UILabel!
    @IBOutlet weak var assetNameLabel: UILabel!
    
    @IBOutlet weak var assetOwnerLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
