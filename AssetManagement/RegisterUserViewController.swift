//
//  RegisterUserViewController.swift
//  AssetManagement
//
//  Created by Seer on 18/07/2018.
//  Copyright © 2018 Seer. All rights reserved.
//

import UIKit

class RegisterUserViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        
        performSegue(withIdentifier: "goToLoginPage", sender: self)
        
    }

    @IBAction func cancelAction(_ sender: UIButton) {
        
        performSegue(withIdentifier: "goToLoginPage", sender: self)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
